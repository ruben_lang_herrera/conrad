<?php
	
$aLang = array(
// Título de la pagina:
"titulo_pagina"         => "Los Muebles de Conrad",
"meta_descripcion"      => "Venta de muebles",

//menu
"menu.quien_es_conrad"  =>"Qui és en Conrad",
"menu.productos"        =>"Productes",
"menu.proyectos"        =>"Projectes",
"menu.condiciones"      =>"Condicions",
"menu.contacto"         =>"Contacte",

 
//listaProductos:
"listaProductos.productos"  =>"Productes",
"listaProductos.todos"      =>"Tots",
"listaProductos.de"         =>"de",

//fichaProductos:
"fichaProductos.relacionados" =>"Productes Relacionats",
"fichaProductos.productos"  =>"Productes /",
"fichaProductos.medidas" =>"Mesures:",
"fichaProductos.stock" =>"Stock:",
"fichaProductos.estado" =>"Estat :",
"fichaProductos.precio" =>"Preu    :",
"fichaProductos.reservar" => "Reservar",
//solucion a los txtos en los campos  numeros.
"fichaProductos.diametro" => "diametra",
"fichaProductos.varios" => "varios",


//conrad 
"conrad.quien_es_conrad" =>"QUI ÉS EN CONRAD?",
"conrad.texto" => " <p>
					   Durant dècades, centenars de contenidors plens de mobles, objectes i roba han quedat a l’oblit per part dels seus propietaris. Ara tenim l'oportunitat de descobrir què guarden en el seu interior.
	               </p>
					<p>
					   El primer contenidor que vam obrir pertanyia a Mr. Conrad, i entre les seves pertinences, descobrim que va ser un emprenedor americà que va fer fortuna. La seva filosofia era vendre qualitat a bon preu. En honor a Mr. Conrad, seguint aquesta màxima, us oferim una selecció d'aquests productes rescatats de l'oblit.
			     	</p>" ,
           
//proyectos
"proyectos.proyectos" => "PROJECTES",
"proyectos.texto"   => "<p>
					       A Conrad viatgem constantment. Si busques alguna peça difícil de trobar a Espanya podem gestionar-te la compra amb algun dels nostres contactes a l'estranger.
	                   </p>
					   <p>
					       També realitzem projectes d'interiorisme per a editorials, escenografies, particulars i comerços. Sempre al més pur estil Conrad.
			     	   </p>",
                        
//condiciones
"condiciones.condiciones" => "CONDICIONS",
"condiciones.titulo" =>  "Condicions de compra",
"condiciones.cond_comp" => "Per a realitzar una compra d'algun dels nostres articles, pots posar-te en contacte amb Conrad ",
"condiciones.realiz_pag" => "Pots realitzar el pagament a través de transferència bancària o en metàl•lic si véns a recollir el producte directament.",
"condiciones.reservas" => "Reserves",
"condiciones.res_art" => "Reserva el teu article tot deixant una paga i senyal.",
"condiciones.envio" => "Enviaments",
"condiciones.entregamos" => "El lliurament el fem arreu d’Espanya. Gestionem el transport a través d'una companyia de confiança. Serà competència del transportista si durant el trajecte hi ha algun problema.",
"condiciones.embalamos" => "Embalem els articles acuradament per al seu òptim transport.",
"condiciones.estado_pieza" => "Estat de les peces",
"condiciones.nuestros_muebles" => "Els nostres mobles són de segona mà i, per tant, tenen l’empremta del pas del temps. Tenim articles restaurats i d’altres que hem preferit deixar-los tal qual són. Fixa't en la descripció del producte.",
"condiciones.mas_fotos" => "Si necessites més fotos o informació més detallada de l'article, no dubtis a contactar amb Conrad.",
"condiciones.devoluciones" => "No acceptem devolucions"  ,

//contacto
"contacto.contacto" => "CONTACTE"        

 


)
?>