<?php
	
$aLang = array(
// Título de la pagina:
"titulo_pagina"         => "Los Muebles de Conrad",
"meta_descripcion"      => "Venta de muebles",

//menu
"menu.quien_es_conrad"  =>"Quién es Conrad",
"menu.productos"        =>"Productos",
"menu.proyectos"        =>"Proyectos",
"menu.condiciones"      =>"Condiciones",
"menu.contacto"         =>"Contacto",

 
//listaProductos:
"listaProductos.productos"  =>"Productos",
"listaProductos.todos"      =>"Todos",
"listaProductos.de"         =>"de",

//fichaProductos:
"fichaProductos.relacionados" =>"Productos Relacionados",
"fichaProductos.productos"  =>"Productos /",
"fichaProductos.medidas" =>"Medidas:",
"fichaProductos.stock" =>"Stock:",
"fichaProductos.estado" =>"Estado:",
"fichaProductos.precio" =>"Precio:",
"fichaProductos.reservar" => "Reservar",
//solucion a los txtos en los campos  numeros.
"fichaProductos.diametro" => "diametra",
"fichaProductos.varios" => "varios",

//conrad 
"conrad.quien_es_conrad" =>"¿QUIEN ES CONRAD?",
"conrad.texto" => " <p>
					   Durante décadas, centenares de contenedores repletos de muebles, objetos y ropa fueron olvidados por sus propietarios. Ahora tenemos la oportunidad de descubrir qué guardan en su interior.
	               </p>
					<p>
					   El primer contenedor que abrimos pertenecía a Mr. Conrad, y entre sus pertenencias descubrimos que fue un emprendedor americano que hizo fortuna. Su filosofía era vender calidad a buen precio. En honor a Mr. Conrad, siguiendo esta máxima, os ofrecemos una selección de estos productos rescatados del olvido.
			     	</p>",
           
//proyectos
"proyectos.proyectos" => "PROYECTOS",
"proyectos.texto"   => "<p>
					       Conrad está viajando constantemente. Si buscas alguna pieza difícil de encontrar en España podemos gestionarte la compra con alguno de nuestros contactos en el extranjero.
	                   </p>
					   <p>
					       También realiza proyectos de interiorismo para editoriales, escenografías, particulares, comercios, etc. Siempre al más puro estilo Conrad.
			     	   </p>",
                        
//condiciones
"condiciones.condiciones" => "CONDICIONES",
"condiciones.titulo" =>  "Condiciones de compra",
"condiciones.cond_comp" => "Para realizar una compra de alguno de nuestros artículos, puedes ponerte en contacto con Conrad",
"condiciones.realiz_pag" => "Puedes realizar el pago a través de transferencia bancaria o en metálico si vienes a recoger el producto directamente.",
"condiciones.reservas" => "Reservas",
"condiciones.res_art" => "Reserva tu artículo dejando una paga y señal.",
"condiciones.envio" => "Envíos",
"condiciones.entregamos" => "Entregamos a toda España. Gestionamos el transporte a través de una compañía de confianza. Será competencia del transportista si durante el trayecto hay algún problema.",
"condiciones.embalamos" => "Embalamos los artículos cuidadosamente para su óptimo transporte.",
"condiciones.estado_pieza" => "Estado de las piezas",
"condiciones.nuestros_muebles" => "Nuestros muebles son de segunda mano y por lo tanto, tienen la huella del paso del tiempo. Tenemos artículos restaurados y otros que hemos preferido dejarlos tal cual. Fíjate en la descripción del producto.",
"condiciones.mas_fotos" => "Si necesitas más fotos o información más detallada del artículo, no dudes en contactar con Conrad.",
"condiciones.devoluciones" => "No aceptamos devoluciones" ,

//contacto
"contacto.contacto" => "CONTACTO"  

                         

 


)
?>