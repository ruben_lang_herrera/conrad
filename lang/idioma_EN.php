<?php
	
$aLang = array(
// Título de la pagina:
"titulo_pagina"         => "Galería Bacelos",
"meta"                  => "galery, art, madrid, vigo, art works, bacelos, expositions",

//menu
"menu.galerias"         =>"Galleries",
"menu.exposiciones"     =>"Exhibitions",
"menu.actuales"         =>"Currents",
"menu.artistas"         =>"Artists",
"menu.news"             =>"News",
"menu.links"            =>"Links",
"menu.contactos"        =>"Contacts",
 
//contactos:
"contacto.titulo"       => "Contacts",

//links
"links.titulo"          => "Links",

//noticias
"noticias.titulo"       => "News",

//artistas
"artistas.exposiciones" => "Exhibitions",
"artistas.obra"         => "Artwork",
"artistas.prensa"       => "Press",

//expos
"lista_expos.titulo"    => "Currents",
"expo.notaPrensa"       => "Press Release",
"expo.imagenes"         => "Images",
"expo.descarga"         => "Download",
"expo.artistasVarios"   => "Various Artists",

//Galería
"galeria.titulo"        =>  "Galería Bacelos",
"galeria.horario"       =>  "Opening Hours",
"galeria.horario.madrid"=>  "Tuesday-Friday: 11 to 19 h.<br/>
                            Saturday: 11 to 14 h.",
"galeria.horario.vigo"  =>  "Tuesday-Friday 10 to 14 hs. / 16.30 to 20.30 hs.<br/>
                            Saturday: 11 a 14 hs.<br/>
                            Closed during August ",

//index
"index.titulo"          =>  "Current Exhibitions", 
"index.titulo.ferias"   =>  "Upcoming  Fairs",

//prensa
"prensa.ver"            =>  "Read More.."
 


)
?>