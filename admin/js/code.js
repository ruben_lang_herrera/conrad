
/* -------------------------------------------------------------------
   // JS du contenu, s'execute partout
--------------------------------------------------------------------*/
$(function() {

/* -------------------------------------------------------------------
   // Hacks
--------------------------------------------------------------------*/

	// Transparence / Pas de fond sous IE 6 et 7 ?

	if ($.browser.msie && $.browser.version<8) {	// Anciens IE trop lents sur transparence

/*		$.getScript('js/jquery.pngFix.pack.js', function(){
			$(document).pngFix();
		});*/

		$('div.info-box, div.info_back, div.info').css('background','none').css('background','#F7F6F1');
	}

	// No-Scroll
	$('html, body').css('overflow','hidden');
	// Scroll vertical uniquement sur petits Ã©crans.
	if ($(window).height()<630) {
		$('html, body').css('overflow-y','auto');
	}

	// Hack : sous IE les nav a 100% passent Ã  la ligne qques instants
	// ++ Evite le dÃ©passement des stitres et des menus.
	var bodyWidth=$('body').width();
	$('#menu div.nav').each(function (index, domEle) {
		var prevWidth=$(domEle).prev('div.btn').width();
		$(this).width(bodyWidth-prevWidth-20+'px');
	});

	// resize de la fenÃªtre
	$(window).bind('resize', function() {
		$('html, body').animate({scrollTop:0}, 'slow').css('overflow','hidden');
		if ($(window).height()<630) {
			$('html, body').css('overflow-y','auto');
		}
		var bodyWidth=$('body').width();
		$('#menu div.nav').each(function (index, domEle) {
			var prevWidth=$(domEle).prev('div.btn').width();
			$(this).width(bodyWidth-prevWidth-20+'px');
		});
		$('div.holder div.nav span span.nav_move:visible').hide();
	})



/* -----------------------------------------------------------------------------
	Fonctions des formulaires
------------------------------------------------------------------------------*/
	/* -----------------------------------------------------------------------------
		Champ "saisissez votre email"
	------------------------------------------------------------------------------*/
	$('INPUT[name=email]').live("click", function()
	{
		if ($(this).val()==$(this).attr('title')) {
			$(this).val('');
		}
	});

	/* -----------------------------------------------------------------------------
		Validation du formulaire
	------------------------------------------------------------------------------*/
	$('#contactsMail INPUT').live("keydown", function(event)
	{
		if (event.keyCode == 13) {
			$('#contactsMail A.submit').click();
			return false;
		}
	});


	$('#contactsMail A.submit').live("click", function()
	{
		var newsEmailOk=false;
		var newsEmail=$('#contactsMail INPUT[name=email]').val();
		var thisPage=$('#contactsMail INPUT[name=page]').val();
		if ((newsEmail.length>7) && (newsEmail.lastIndexOf(".") > 2) && (newsEmail.indexOf("@") > 0) && (newsEmail.lastIndexOf(".") > (newsEmail.indexOf("@")+1)) && (newsEmail.indexOf("@") == newsEmail.lastIndexOf("@") && (newsEmail.lastIndexOf(".")<(newsEmail.length-2))))
		{
			$.post("ajax/actions.php",
			{
				ajaxAction: 'contactsMail'
				,email: newsEmail
				,page: thisPage
			});
			var newsEmailOk=true;
		}
		if (newsEmailOk==true) {
			$('#contactsMail').before("<h2>Merci</h2>").hide();
		}
		else
		{
			$('#contactsMail INPUT[name=email]').css('border','1px solid red');
		}
		return false;
	});

/* -----------------------------------------------------------------------------
	Zooms Produits
------------------------------------------------------------------------------*/
	$('div.product-list div IMG').live("mouseover", function(){
		$(this).parent('div').removeClass('iphone').addClass('iphone-on');
		$(this).attr('height','295').attr('width','168');
	}).live("mouseout", function(){
		$(this).parent('div').removeClass('iphone-on').addClass('iphone');
		$(this).attr('height','215').attr('width','123');
	});


});