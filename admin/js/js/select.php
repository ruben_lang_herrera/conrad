<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<table  id="table_transp_select1"><tr><td/></tr></table>
<div id="pager_transp_select1"></div> 



<script type="text/javascript">
 $(document).ready(function(){
 var lastsel;
 $(function(){ 
 $("#table_transp_select1").jqGrid({
                    url:'transportista.php',
                    datatype: 'xml',
                    mtype: 'GET',
                    
                       colNames:[
                       
                                "Sel",
                                "ordre",
                                "codi",
                                "nom",
                                "entitat",
                                "oficina",
                                "conrtol",
                                "ncompte",
                                "nif",
                                "adressa", 
                                "cp", 
                                "poblacio", 
                                "telefon", 
                                "mobil", 
                                "itc", 
                                "notes", 
                                "tpagament", 
                                "naixement", 
                                "ambit", 
                                "remolc", 
                                "marca", 
                                "tremolc", 
                                "ADR", 
                                "idclient", 
                                "alta", 
                                "nom2", 
                                "canvinom", 
                                "empresa", 
                                "tarifa", 
                                "adreca", 
                                "tpag2", 
                                "entitat2", 
                                "oficina2", 
                                "control2", 
                                "ncompte2", 
                                "idclient2", 
                                "datacanvi", 
                                "socivinculat", 
                                "canviactiu", 
                                "PASS", 
                                "EXTRA", 
                                "TOTAL", 
                                "VIAMARITIMA" 
                                ],
                               
                               
                   
                    colModel :[ 
                    
                      {name:'act',index:'act', width:75,sortable:false},
                      {name:'ordre', index:'ordre', width:55, align:'right', editable:false}, 
                      {name:'codi', index:'codi', width:55, align:'right', editable:false}, 
                      {name:'nom', index:'nom', width:100, search:true, editable:true, editrules:{edithidden:true,required:true} }, 
                      {name:'entitat', index:'entitat', width:50 , editable:true,editrules:{edithidden:true,number:true}},
                      {name:'oficina', index:'oficina', width:50 , editable:true,editrules:{edithidden:true,number:true}},
                      {name:'conrtol', index:'conrtol', width:50 , editable:true,editrules:{edithidden:true,number:true}},
                      {name:'ncompte', index:'ncompte', width:50 , editable:true,editrules:{edithidden:true,number:true}},
                      {name:'nif', index:'nif', width:80, sortable:false, editable:true},
                      {name:'adressa', index:'adressa', width:170 , editable:true},
                      {name:'cp', index:'cp', width:55 , editable:true,editrules:{edithidden:true,number:true}},
                      {name:'poblacio', index:'poblacio', width:100, editable:true},
                      {name:'telefon', index:'telefon', width:90 , editable:true,editrules:{edithidden:true,number:true}},
                      {name:'mobil', index:'mobil', width:90 , editable:true,editrules:{edithidden:true,number:true}},
                      {name:'itc', index:'itc', width:55,align:'center',editable:true,edittype:"checkbox",editoptions:{value:"S:N"},search:false},
                      {name:'notes', index:'notes', width:170 , editable:true},
                      {name:'tpagament', index:'tpagament', width:70, editable: true, edittype:"select",editoptions:{value:"Transfer:Transfer;Pagar:Pagar;Socios:Socios"}},
                      {name:'naixement', index:'naixement', width:55 , editable:true, editoptions:{size:10},formoptions:{elmsuffix:" yyyy-mm-dd"}},//rowpos:2,
                      {name:'ambit', index:'ambit', width:55 , editable: true, edittype:"select",editoptions:{value:"NACIONAL:NACIONAL;LOCAL:LOCAL;COMARCAL:COMARCAL"}},
                      {name:'remolc', index:'remolc', width:55 , editable:true},
                      {name:'marca', index:'marca', width:55 , editable:true},
                      {name:'tremolc', index:'tremolc', width:55 , editable:true},
                      {name:'ADR', index:'ADR', width:55,align:'center',editable:true,edittype:"checkbox",editoptions:{value:"S:N"},search:false},
                      {name:'idclient', index:'idclient', width:55 , editable:true},
                      {name:'alta', index:'alta', width:55 , editable:true, editoptions:{size:10},formoptions:{elmsuffix:" yyyy-mm-dd"}},
                      {name:'nom2', index:'nom2', width:55 , editable:true},
                      {name:'canvinom', index:'canvinom',width:55,align:'center',editable:true,edittype:"checkbox",editoptions:{value:"S:N"},search:false},
                      {name:'empresa', index:'empresa', width:55 , editable:true},
                      {name:'tarifa', index:'tarifa', width:55 , editable:true},
                      {name:'adreca', index:'adreca', width:170 , editable:true},
                      {name:'tpag2', index:'tpag2', width:55 , editable:true},
                      {name:'entitat2', index:'entitat2', width:55 , editable:true},
                      {name:'oficina2', index:'oficina2', width:55 , editable:true},
                      {name:'control2', index:'control2', width:55 , editable:true},
                      {name:'ncompte2', index:'ncompte2', width:55 , editable:true},
                      {name:'idclient2', index:'idclient2', width:55 , editable:true},
                      {name:'datacanvi', index:'datacanvi', width:55 , editable:true},
                      {name:'socivinculat', index:'socivinculat', width:55 , editable:true},
                      {name:'canviactiu', index:'canviactiu', width:55 , editable:true},
                      {name:'PASS', index:'PASS', width:55 , editable:true},
                      {name:'EXTRA', index:'EXTRA', width:55,align:'center',editable:true,edittype:"checkbox",editoptions:{value:"1:0"},search:false},
                      {name:'TOTAL', index:'TOTAL', width:55 , editable:true},
                      {name:'VIAMARITIMA', index:'VIAMARITIMA', width:55 , editable:true}
                      
                      ],
                      
                    pager: '#pager_transp_select1',
                    
                    rowNum:10,
                    //scrowl
                    rowList:[10,20,30],
                    sortname: 'codi',
                    sortorder: 'DESC',
                    onSelectRow: function(rowid,selected){ 
            
                            if(rowid ){ 
                                $('#table_transp_select').jqGrid('restoreRow',lastsel); 
                                //$('#table_transp_select').jqGrid('editRow',rowid,true); 
                                //lastsel=rowid; 
                            //======================================
                              
                               
                                $('#tab_sec').css('visibility', 'visible');



                            
                                //var gsr = $("#table_camions").jqGrid('getGridParam','selrow');
                                //$("#table_camions").jqGrid('GridToForm',gsr,"#order");
                            //======================================
                                } 


                            },
                   /* ondblClickRow: function(id){ 
                                    
                                alert ('adad');
                                if(id){ 
                                //$('#table_transp_select').jqGrid('restoreRow',lastsel);
                                //alert( $('#sel_soc').attr("sel"));
                                //$('#table_transp_select').jqGrid('editRow',rowid,true); 
                                lastsel=id;
                                }

                    },*/
                   /* gridComplete: function(){ 
                                alert('as');
                                var ids = jQuery("#table_transp_select").jqGrid('getDataIDs'); 
                                for(var i=0;i < ids.length;i++){ 
                                    var cl = ids[i]; 
                                    se = "<input style='height:22px;width:20px;' type='button' value='S' onclick=\"jQuery('#table_transp_select').saveRow('"+cl+"');\" />"; 
                                    
                                    jQuery("#table_transp_select").jqGrid('setRowData',ids[i],{act:se}); 
                                    } 
                               
                                },*/
                    viewrecords: true,
                    gridview: true,
                    caption: '<div>SOCIOS</div>',
                    height: 'auto',
                    width: '880',
                    shrinkToFit: false,
                    editurl: "transportista.php",
                    postData:{"oper":"grid"},
                    reloadAfterSubmit:true
                    
                    
                    
                    
                    
                  }); 
                 
                $("#table_transp_select").filterToolbar({ searchOnEnter:true }); 
                $("#table_transp_select").jqGrid('navGrid','#pager_transp_select',{});
                $("#table_transp_select").jqGrid('hideCol',["ordre"]);
              
             
             
             
                
                
 });
 
                
                 
 
 });         
</script>

