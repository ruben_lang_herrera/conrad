<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of db_obj
 *
 * @author ruben lang
 */



class db_obj
{

    private $dbhost = "localhost"; //slgg894.piensasolutions.com
    private $database = "qom120";
    private $dbuser = "qom120";
    private $dbpassword = "Conrad2012"; //


    var $table;
    var $dbmap = array();
    var $pr_key = "";
    /*
    *
   "event_id"=>"id", // this is a id key
   "title"=>"title",
   "start"=>"start",
   "end"=>"end",
   "description"=>"description",
   "location"=>"location",
   "categories"=>"className",
   "access"=>"access",
   "all_day"=>"allDay",
   "user_id"=>"user_id"

    */
    private $encoding = "utf-8";
    private $dbtype; //pensar que hacer con esto
    //
    private $searchwhere = "";
    private $searchdata = array();

    private $wherecond = ""; // Ex. "WHERE id = 2"  or "WHERE nombre LIKE %rub%" or "WHERE nif=2 AND pelo = rojo"
    private $whereparams = array();

    private $count = 0; //cantidad de filas de la ultima consulta
    private $result = array(); //resultado de la ultima consulta


//functions

    protected function __construct($table, $map, $key)
    {

        $this->table = $table;
        $this->dbmap = $map;
        $this->pr_key = $key;
    }

    /*
     public function setUser( $user) {
         if($user) {
             $this->user_id=$user;
         }
     }
     public function setTable( $table) {
         if($table) {
             $this->table=$table;
         }
     }

    */
    protected function setDbType($dtype)
    {
        $this->dbtype = $dtype;
    }

    protected function setSearchs($where, array $whrval)
    {
        $this->searchwhere = $where;
        $this->searchdata = $whrval;
    }

    protected function setWhere($where) //wherepart = array();
    {
        $this->wherecond = $where;
        //$this->whereparams = $param;
    }

    protected function getCount() //wherepart = array();
    {
        return $this->count;
        //$this->whereparams = $param;
    }

    protected function getResult() //wherepart = array();
    {
        return $this->result;
        //$this->whereparams = $param;
    }


    private function connect_db()
    { //dbconfig.php

        $db = mysql_connect($this->dbhost, $this->dbuser, $this->dbpassword) or die("Connection Error: " . mysql_error());

        // select the database
        mysql_select_db($this->database) or die("Error connecting to db.");
        
        return $db;
    }


    public function insert($data = array()) //inserta una nueva fila en la base de datos
    {
        //$start, $end, $title, $description, $location, $categories, $access, $allDay) {


        $tableFields = $this->dbmap; //modificar aqui para solo entrar los table keys

        array_splice($tableFields, 0, 1); //quito la pr_key


        $binds = array();


        $rowFields = array_intersect($tableFields, array_keys($data));


        foreach ($rowFields as $key => $val) {


            $value = $data[$val];
         
            if (strtolower($this->encoding) != 'utf-8') {
                $value = iconv("utf-8", $this->encoding . "//TRANSLIT", $value);
            }
            
            $binds[] = "'" .$value. "'";
        }
        $sql = "";

        $sql = "INSERT INTO " . $this->table .
            " ( `" . implode("`, `", $rowFields) . "` )" .
            " VALUES ( " . implode(', ', $binds) . ")";

        echo $sql;

        if (!$sql) return false;

        mysql_query("SET NAMES 'utf8'");

        $this->connect_db();
        //$db->set_charset("utf8");


        if (!mysql_query($sql))
            return false;

        /*

jqGridDB::beginTransaction($this->db);
$query = jqGridDB::prepare($this->db, $sql, $binds);
jqGridDB::execute($query);
$lastid = jqGridDB::lastInsertId($this->db, $this->table, 'event_id', $this->dbtype);
jqGridDB::commit($this->db);
jqGridDB::closeCursor($query);
return $lastid;
        *
        */

        return true;

    }

    public function update($data = array())
    {


        $search = array();
        $count = 300;

        $rowFields = array_intersect($this->dbmap, array_keys($data));

        //echo implode(', ', $data) ;

        $binds = array();
        $updateFields = array();
        //$pk = 'event_id';
        foreach ($rowFields as $key => $field) {

            $value = $data[$field];

            if (strtolower($this->encoding) != 'utf-8') {

                $value = iconv("utf-8", $this->encoding . "//TRANSLIT", $value);
            }
            if ($field != $this->pr_key) {

                $updateFields[] = "`" . $field . "`  = ?$count ";

                $binds[] = "'" . $value . "'";
                $search[] = "?$count";


            } else if ($field == $this->pr_key) {
                $v2 = $value;
            }
            $count++;

        }
        //echo implode(', ', $updateFields);  debug
        //echo implode(', ', $binds);
        //echo implode(', ', $search);

        if (!isset($v2)) die("Primary value is missing");
        $binds[] = $v2;

        $sql = "";
        if (count($updateFields) > 0) {
            $sql = "UPDATE " . $this->table .
                " SET " . implode(', ', $updateFields) .
                " WHERE `" . $this->pr_key . "` = '" . $v2 . "'";
            // Prepare update query
        }


        $sql = str_replace($search, $binds, $sql);

        echo $sql;   //debug

        $this->connect_db();

        mysql_query("SET NAMES 'utf8'");

        if (!mysql_query($sql))
            return false;

        return true;

    }

    public function delete($id)
    {


        $sql = "DELETE FROM " . $this->table . " WHERE " . $this->pr_key . " = " . $id;
        echo $sql;

        if (!$sql) {
            return false;
        }

        $this->connect_db();

        if (!mysql_query($sql))
            return false;

        return true;


    }

    public function select($start = "0", $end = "10", $sidx = "", $sord = "DESC")
    {
        
        unset($this->result); //libero lo que tenia en resultados 
        
        if (!$sidx)
            $sidx = $this->pr_key;


        $sql = "SELECT " . implode(', ', $this->dbmap);
   
        $sql .= ' FROM ' . $this->table . ' ' . $this->wherecond . ' ORDER BY ' . $sidx . ' ' . $sord . ' LIMIT ' . $start . ' , ' . $end;


        $this->connect_db();

        mysql_query("SET NAMES 'utf8'");

        $result = mysql_query('SELECT COUNT(*) AS count FROM ' . $this->table . ' ' . $this->wherecond); //cuenta filas de consulta

        $row = mysql_fetch_array($result, MYSQL_ASSOC);

        $this->count = $row['count'];
        
        mysql_free_result($result); //free memory 
        
        $result = mysql_query($sql) or die("Couldn't execute query." . mysql_error());

        $i = 1;

        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {

            foreach ($row as $k => $v) {

                $this->result[$i][$k] = $v;

            }

            $i++;
        }
        mysql_free_result($result); //libera la memoria utilizada por la consulta para evitar memory leaks
       
        $this->wherecond = ''; //limpio los parametros de busqueda

        return $sql; //para debugg

    }
    
public function select_free($sql)  //el sql tienen toda la consulta
    {
        //echo $sql;
        unset($this->result); //libero lo que tenia en resultados 
        
        $this->connect_db();

        mysql_query("SET NAMES 'utf8'");
        //echo $sql;
        
        $result = mysql_query($sql) or die("Couldn't execute query." . mysql_error());

        $i = 1;

        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {

            foreach ($row as $k => $v) {

                $this->result[$i][$k] = $v;

            }

            $i++;
        }

        mysql_free_result($result); //libera la memoria utilizada por la consulta para evitar memory leaks

        return $sql; //para debugg

    }

    protected function GET_MAX($colum_name)
    {
        $this->connect_db();
        mysql_query("SET NAMES 'utf8'");

        $sql = 'SELECT MAX( ' . $colum_name . ') AS  MAX_CODI  FROM ' . $this->table;
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result, MYSQL_ASSOC);

        return $row['MAX_CODI'];

    }
}

?>
