<?php

/**
 * @author ruben
 * @copyright 2012
 */
 
 
include_once('db_obj.php');

function encoder_characters($string) { //utf-8
    utf8_encode($string);
    $string = urlencode($string);
    
    return $string;
}
function decoder_characters($string) { 
    utf8_encode($string);    
    
    $a = array("&","<",">",'"');                //jqgrid no reconoce estos caracteres 
    $b = array("&amp;","&lt;","&gt;","&quot;");
    
    $string = urldecode($string);
    $string = str_replace($a,$b,$string);
    
    return $string;
}
function decoder_characters_front($string) { //utf-8
    utf8_encode($string);
    $string = urldecode($string);
    $string = nl2br($string);
    
    return $string;
}

/*** Stock de clases **/
class categorias extends db_obj {
    
    private $table_name='categorias';
    
    private $map = array (
            
          'id', 'nombre_ES', 'nombre_CA'
            
               );
    
    private $key='id'; 
    
    private $ord='ASC';
    private $end='10';
    private $sidx='1'; 
    
    //paginado
    private $page;
    
    private $where = array();
    private $count = 0;
    
    public function __construct() {
        
        parent::__construct($this->table_name, $this->map, $this->key);
    }
    
     public function set_ord($o)
    {
        $this->ord=$o;
    }
    
    public function set_end($e)
    {
        $this->end=$e;
    }
    
    public function set_sidx($s)
    {
        $this->sidx=$s;
    }

    public function set_page($p)
    {
        $this->page=$p;
    }
    
     public function add($data = array() ){
        
        if($this->insert($data))
            return true;
        return false;
        
      
        
    }
    
    public function delete($id) {
        parent::delete($id);
    
    }

    
    public function edit($data = array() ){
       
        if($this->update($data))
            return true;
        return false;
        
    }
    
    public function last_id(){
        
        return $this->GET_MAX($this->key);
    }
    
    public function get_cont()
    {
        return $this->count;
    } 
    
     public function get_datos($busqueda=""){  //sentencia WHERE ESPECIFICA 
        $row = array();

        if($busqueda!="")
            $this->setWhere($busqueda);
     
        $this->select(0, $this->last_id(),1 ,"ASC");
        $row=$this->getResult();
        
        $this->count =  count($row);
        return $row;
       }
       
      public function get_any($sql)
     {
        $row = array();
        $this->select_free($sql);
        $row=$this->getResult();
        $this->count =  count($row);                
        return  $row;
     }
    
    
      
    
     public function get_grid($busqueda=""){  //sentencia WHERE ESPECIFICA (usa codi como ID de fila)
        
        
        $row = array();
        
        $start = $this->end * $this->page - $this->end ;
        if($start <0) $start = 0; 
        
        if($busqueda!="")
            $this->setWhere($busqueda);
        
        $this->select($start, $this->end, $this->sidx, $this->ord);
        
        $row=$this->getResult();
        
        $count=  $this->getCount();
        
        if( $count > 0 && $this->end > 0) { 
              $total_pages = ceil($count/$this->end); 
        } else { 
                    $total_pages = 0; 
        } 
 
        if ($this->page > $total_pages) 
              $this->page=$total_pages;
        
        if($count >($this->end-$start))
            $c=$this->end-$star;
        else
            $c=$count;
        
        
        header("Content-type: text/xml;charset=utf-8"); //no se para que es pero dice la documentacion q hay q ponerlo

        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>". $this->page."</page>";
        $s .= "<total>".$total_pages."</total>";
        $s .= "<records>".$count."</records>";

        
        while($c) {
            
            $s .= "<row id='". $row[$c][$this->key]."'>";
            foreach ($this->map as $key => $value) {
                
                $s .= "<cell>". decoder_characters($row[$c][$value])."</cell>";    
                
            }
            $s .= "</row>";
            $c--;
        }
        $s .= "</rows>"; 
        
       return $s;
    }
    
    
}



class usuarios extends db_obj {
    
    private $table_name='usuarios';
    
    private $map = array (
            
                'id','email', 'password'
                
                            
                );
    
    private $key='id'; 
    
    private $ord='ASC';
    private $end='10';
    private $sidx='1'; 
    
    //paginado
    private $page;
    
    private $where = array();
    private $count = 0;
    
    public function __construct() {
        
        parent::__construct($this->table_name, $this->map, $this->key);
    }
    
     public function set_ord($o)
    {
        $this->ord=$o;
    }
    
    public function set_end($e)
    {
        $this->end=$e;
    }
    
    public function set_sidx($s)
    {
        $this->sidx=$s;
    }

    public function set_page($p)
    {
        $this->page=$p;
    }
    
     public function add($data = array() ){
        
        if($this->insert($data))
            return true;
        return false;
        
      
        
    }
    
    public function delete($id) {
        parent::delete($id);
    
    }

    
    public function edit($data = array() ){
       
        if($this->update($data))
            return true;
        return false;
        
    }
    
    public function last_id(){
        
        return $this->GET_MAX($this->key);
    }
    
    public function get_cont()
    {
        return $this->count;
    } 
    
     public function get_datos($busqueda=""){  //sentencia WHERE ESPECIFICA 
        $row = array();

        if($busqueda!="")
            $this->setWhere($busqueda);
     
        $this->select(0, $this->last_id(),1 ,"ASC");
        $row=$this->getResult();
        
        $this->count =  count($row);
        return $row;
       }
       
    public function check_user($user,$pass){
        
        $this->select_free('SELECT * FROM '.$this->table_name.' WHERE email= "'.$user.'" and password= "'.$pass.'"');
        return count($this->getResult())? true: false;
    }
    
     public function get_grid($busqueda=""){  //sentencia WHERE ESPECIFICA (usa codi como ID de fila)
        
        
        $row = array();
        
        $start = $this->end * $this->page - $this->end ;
        if($start <0) $start = 0; 
        
        if($busqueda!="")
            $this->setWhere($busqueda);
        
        $this->select($start, $this->end, $this->sidx, $this->ord);
        
        $row=$this->getResult();
        
        $count=  $this->getCount();
        
        if( $count > 0 && $this->end > 0) { 
              $total_pages = ceil($count/$this->end); 
        } else { 
                    $total_pages = 0; 
        } 
 
        if ($this->page > $total_pages) 
              $this->page=$total_pages;
        
        if($count >($this->end-$start))
            $c=$this->end-$star;
        else
            $c=$count;
        
        
        header("Content-type: text/xml;charset=utf-8"); //no se para que es pero dice la documentacion q hay q ponerlo

        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".  $this->page."</page>";
        $s .= "<total>".$total_pages."</total>";
        $s .= "<records>".$count."</records>";

        
        while($c) {
            
            $s .= "<row id='". $row[$c][$this->key]."'>";
            foreach ($this->map as $key => $value) {
                
                $s .= "<cell>". decoder_characters($row[$c][$value])."</cell>";     
                
            }
            $s .= "</row>";           
            
            $c--;
        }
        $s .= "</rows>"; 
        
       return $s;
    }
    
    
}

class productos extends db_obj {
    
    private $table_name='productos';
    
    private $map = array (
            
               'id', 
                'nombre_ES', 
                'nombre_CA', 
                'ref', 
                'descripcion_ES', 
                'descripcion_CA', 
                'medidas', 
                'stock', 
                'estado_ES', 
                'estado_CA', 
                'precio', 
                'nuevo',
                'vendido',
                'url',                
                'id_categoria'
                
                            
                );
    
    private $key='id'; 
    
    private $ord='ASC';
    private $end='10';
    private $sidx='1'; 
    
    //paginado
    private $page;
    
    private $where = array();
    private $count = 0;
    
    public function __construct() {
        
        parent::__construct($this->table_name, $this->map, $this->key);
    }
    
     public function set_ord($o)
    {
        $this->ord=$o;
    }
    
    public function set_end($e)
    {
        $this->end=$e;
    }
    
    public function set_sidx($s)
    {
        $this->sidx=$s;
    }

    public function set_page($p)
    {
        $this->page=$p;
    }
    
     public function add($data = array() ){
        
        if($this->insert($data))
            return true;
        return false;
        
      
        
    }
    
    public function delete($id) {
        parent::delete($id);
    
    }

    
    public function edit($data = array() ){
       
        if($this->update($data))
            return true;
        return false;
        
    }
    
    public function last_id(){
        
        return $this->GET_MAX($this->key);
    }
    
    public function get_cont()
    {
        return $this->count;
    } 
    
     public function get_datos($busqueda=""){  //sentencia WHERE ESPECIFICA 
        $row = array();

        if($busqueda!="")
            $this->setWhere($busqueda);
            
        $start = $this->end * $this->page - $this->end ;
        if($start <0) $start = 0;         
        
        $this->select($start, $this->end, $this->sidx, $this->ord);
        $row=$this->getResult();
        
        $this->count =  count($row);
        return $row;
       }
       
    public function check_user($user,$pass){
        
        $this->select_free('SELECT * FROM '.$this->table_name.' WHERE email= "'.$user.'" and password= "'.$pass.'"');
        return count($this->getResult())? true: false;
    }
    
     public function get_grid($busqueda=""){  //sentencia WHERE ESPECIFICA (usa codi como ID de fila)
        
        
        $row = array();
        
        $start = $this->end * $this->page - $this->end ;
        if($start <0) $start = 0; 
        
        if($busqueda!="")
            $this->setWhere($busqueda);
        
        $this->select($start, $this->end, $this->sidx, $this->ord);
        
        $row=$this->getResult();
        
        $count=  $this->getCount();
        
        if( $count > 0 && $this->end > 0) { 
              $total_pages = ceil($count/$this->end); 
        } else { 
                    $total_pages = 0; 
        } 
 
        if ($this->page > $total_pages) 
              $this->page=$total_pages;
        
        if($count >($this->end-$start))
            $c=$this->end-$star;
        else
            $c=$count;
        
        
        header("Content-type: text/xml;charset=utf-8"); //no se para que es pero dice la documentacion q hay q ponerlo

        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".  $this->page."</page>";
        $s .= "<total>".$total_pages."</total>";
        $s .= "<records>".$count."</records>";

        
        while($c) {
            
            $s .= "<row id='". $row[$c][$this->key]."'>";
            foreach ($this->map as $key => $value) {
                
                $s .= "<cell>". decoder_characters($row[$c][$value])."</cell>";     
                
            }
            $s .= "</row>";           
            
            $c--;
        }
        $s .= "</rows>"; 
        
       return $s;
    } 
    
  public function get_total($categoria){
    
    
    if($categoria!=0){
        $this->select_free('select * from productos where id_categoria= '.$categoria);
        }
    else {
        $this->select_free('select * from productos');
       }
      return count($this->getResult());
    
  } 
    
    
}

class imagenes extends db_obj {
    
    private $table_name='imagenes_slide';
    
    private $map = array (
            
             'id',
             'titulo_ES',
             'titulo_CA',
             'descripcion_ES',
             'descripcion_CA',
             'url'
                
                            
                );
    
    private $key='id'; 
    
    private $ord='ASC';
    private $end='10';
    private $sidx='1'; 
    
    //paginado
    private $page;
    
    private $where = array();
    private $count = 0;
    
    public function __construct() {
        
        parent::__construct($this->table_name, $this->map, $this->key);
    }
    
     public function set_ord($o)
    {
        $this->ord=$o;
    }
    
    public function set_end($e)
    {
        $this->end=$e;
    }
    
    public function set_sidx($s)
    {
        $this->sidx=$s;
    }

    public function set_page($p)
    {
        $this->page=$p;
    }
    
     public function add($data = array() ){
        
        if($this->insert($data))
            return true;
        return false;
        
      
        
    }
    
    public function delete($id) {
        parent::delete($id);
    
    }

    
    public function edit($data = array() ){
       
        if($this->update($data))
            return true;
        return false;
        
    }
    
    public function last_id(){
        
        return $this->GET_MAX($this->key);
    }
    
    public function get_cont()
    {
        return $this->count;
    } 
    
     public function get_datos($busqueda=""){  //sentencia WHERE ESPECIFICA 
        $row = array();

        if($busqueda!="")
            $this->setWhere($busqueda);
     
        $this->select(0, $this->last_id(),1 ,"ASC");
        $row=$this->getResult();
        
        $this->count =  count($row);
        return $row;
       }
       
    public function check_user($user,$pass){
        
        $this->select_free('SELECT * FROM '.$this->table_name.' WHERE email= "'.$user.'" and password= "'.$pass.'"');
        return count($this->getResult())? true: false;
    }
    
     public function get_grid($busqueda=""){  //sentencia WHERE ESPECIFICA (usa codi como ID de fila)
        
        
        $row = array();
        
        $start = $this->end * $this->page - $this->end ;
        if($start <0) $start = 0; 
        
        if($busqueda!="")
            $this->setWhere($busqueda);
        
        $this->select($start, $this->end, $this->sidx, $this->ord);
        
        $row=$this->getResult();
        
        $count=  $this->getCount();
        
        if( $count > 0 && $this->end > 0) { 
              $total_pages = ceil($count/$this->end); 
        } else { 
                    $total_pages = 0; 
        } 
 
        if ($this->page > $total_pages) 
              $this->page=$total_pages;
        
        if($count >($this->end-$start))
            $c=$this->end-$star;
        else
            $c=$count;
        
        
        header("Content-type: text/xml;charset=utf-8"); //no se para que es pero dice la documentacion q hay q ponerlo

        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .=  "<rows>";
        $s .= "<page>".  $this->page."</page>";
        $s .= "<total>".$total_pages."</total>";
        $s .= "<records>".$count."</records>";

        
        while($c) {
            
            $s .= "<row id='". $row[$c][$this->key]."'>";
            foreach ($this->map as $key => $value) {
                
                $s .= "<cell>". decoder_characters($row[$c][$value])."</cell>";     
                
            }
            $s .= "</row>";           
            
            $c--;
        }
        $s .= "</rows>"; 
        
       return $s;
    } 
    
    
}




?>