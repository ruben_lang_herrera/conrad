
<script type="text/javascript">
$(document).ready(function(){
var lastsel;
$(function(){ 
  $("#table_imagenes").jqGrid({
    url:'ajax/imagenes.php',
    datatype: 'xml',
    mtype: 'GET',
    colNames:[
             
             'No.',             
             'Titulo ESP',
             'Titulo CAT',
             'Descripcion ESP',
             'Descripcion CAT',
             'Url'
             ],
             
    colModel :[ 
    
              {name:'id', index:'id', width:120, align:'right', editable:false}, 
              {name:'titulo_ES', index:'titulo_ES', width:325, align:'right', editable:true, editrules:{required:true}},
              {name:'titulo_CA', index:'titulo_CA', width:262, align:'right', editable:true, editrules:{required:true}},
              {name:'descripcion_ES', index:'descripcion_ES', width:350, editable:true,edittype:"textarea", editoptions:{rows:"4",cols:"30"}},
              {name:'descripcion_CA', index:'descripcion_CA', width:350, editable:true,edittype:"textarea", editoptions:{rows:"4",cols:"30"}},
              {name:'url', index:'url', width:325, align:'right', editable:true}
            ],
            
    pager: '#pager_imagenes',
    
    rowNum:10,
    rowList:[10,20,30],
    sortname: 'id',
    sortorder: 'desc',
    onSelectRow: function(rowid,selected){ 
            
            if(rowid && rowid!==lastsel){ 
                $('#table_imagenes').jqGrid('restoreRow',lastsel); 
                //jQuery('#table_imagenes').jqGrid('editRow',rowid,true); 
                lastsel=rowid;                
                var gsr = $("#table_imagenes").jqGrid('getGridParam','selrow');
                $("#table_imagenes").jqGrid('GridToForm',gsr,"#order");
                
                
                
            //======================================
                $('.qq-upload-button ').show();
                } 
                
                
            },
    ondblClickRow: function(rowid){ 
        
                if(rowid){ 
                 
                $('#table_imagenes').jqGrid('editRow',rowid,true); 
                lastsel=rowid;
                }
            
    },
            
    viewrecords: true,
    gridview: true,
    caption: '<div>imageness</div>',
    height: 'auto',
    editurl: "ajax/imagenes.php",
    postData:{"oper":"grid"},
    width: '660',
    shrinkToFit: false,    
    reloadAfterSubmit:true
    
    
    
    
    
  }); 
  


    $("#table_imagenes").filterToolbar({searchOnEnter:true }); 

    function processAddEdit(response, postdata) {
        	var success = true;
        	var message = ""
        	var new_id = "1";
            alert("Guardado");
        	return [success,message,new_id];
        }
    

    $("#table_imagenes").jqGrid('navGrid','#pager_imagenes',
        {search:false},
        
        {
    		//afterShowForm:afterShowEdit, 
    		//afterSubmit:processAddEdit,
    		//beforeSubmit:validateData,
            beforeShowForm: function(form) { 
                
                 $('#tr_descripcion_ES', form).show();               
                 $('#tr_descripcion_CA', form).show();
                 $('#tr_titulo_CA', form).show();
                 $('#tr_url', form).hide();
                 
              
                
                
               
            },
    		closeAfterEdit: true
    	},
        {   
            beforeShowForm: function(form) { 
                
                 $('#tr_descripcion_ES', form).show();               
                 $('#tr_descripcion_CA', form).show();
                 $('#tr_titulo_CA', form).show();
                 $('#tr_url', form).hide();
            },
            closeAfterAdd : true,  
            afterSubmit:processAddEdit
        }
        );

    
    
   $("#table_imagenes").jqGrid('hideCol',["id","descripcion_ES","descripcion_CA","titulo_CA"]);  //escondo las colomnas

   // jQuery("#table_imagenes").jqGrid('navButtonAdd','#pager_proyecto');
   
    var last;
    var last_grid;
    
    $("#order .c_left").change(function () {
    var ord = $("#table_imagenes").jqGrid('getGridParam','selrow');
        last = ord;
        
        if(ord) { 
            
            last_grid = $("#table_imagenes").jqGrid('getGridParam',ord);
            
            $("#table_imagenes").jqGrid('FormToGrid',ord,"#order");
            $("#table_imagenes").jqGrid('editRow',ord);  //'editGridRow' me llama al dialogo de 
            //this.disabled = 'true'; 
            jQuery("#save,#cancel").attr("disabled",false); 
            $(this).focus();
            
        }
    });
    

    
  
    

    
    
        
    
    

    });





    // LLamo al Script de Upload que está en admin.php
    createUploader("imagenes");


          
 


});  








</script>

     
    <div id="mysearch"></div>
    <table  id="table_imagenes"><tr><td/></tr></table> 
    <div id="pager_imagenes"></div>  
    
    
    
    

