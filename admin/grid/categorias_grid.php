
<script type="text/javascript">
$(document).ready(function(){
var lastsel;
$(function(){ 
  $("#table_categorias").jqGrid({
    url:'ajax/categorias.php',
    datatype: 'xml',
    mtype: 'GET',
    colNames:[
                       'No.', 
                       'Nombre ESP',
                       'Nombre CAT'
             ],
             
    colModel :[ 
    
              {name:'id', index:'id', width:120, align:'right', editable:false}, 
              {name:'nombre_ES', index:'nombre_ES', width:285, align:'right', editable:true, editrules:{required:true}},
              {name:'nombre_CA', index:'nombre_CA', width:285, align:'right', editable:true, editrules:{required:true}}
              
            ],
            
    pager: '#pager_categorias',
    
    rowNum:10,
    rowList:[10,20,30],
    sortname: 'id', 
    sortorder: 'desc',
    onSelectRow: function(rowid,selected){ 
            
            if(rowid && rowid!==lastsel){ 
                $('#table_categorias').jqGrid('restoreRow',lastsel); 
                //jQuery('#table_categorias').jqGrid('editRow',rowid,true); 
                lastsel=rowid; 
            //======================================
                
                
                var gsr = $("#table_categorias").jqGrid('getGridParam','selrow');
                $("#table_categorias").jqGrid('GridToForm',gsr,"#order");
                
            //======================================
                } 
                
                
            },
             ondblClickRow: function(rowid){ 
        
                if(rowid){ 
                 
                $('#table_categorias').jqGrid('editRow',rowid,true); 
                lastsel=rowid;
                }
            
    },
            
    viewrecords: true,
    gridview: true,
    caption: '<div>Categorías</div>',
    height: 'auto',
    editurl: "ajax/categorias.php",
    postData:{"oper":"grid"},
    width: '580',
    shrinkToFit: false,    
    reloadAfterSubmit:true
    
    
    
    
    
  }); 
  


    $("#table_categorias").filterToolbar({searchOnEnter:true }); 

    function processAddEdit(response, postdata) {
        	var success = true;
        	var message = ""
        	var new_id = "1";
            alert("Guardado");
        	return [success,message,new_id];
        }
    

    $("#table_categorias").jqGrid('navGrid','#pager_categorias',
        {search:false},
        
        {
    		//afterShowForm:afterShowEdit, 
    		//afterSubmit:processAddEdit,
    		//beforeSubmit:validateData,
    		
    		closeAfterEdit: true
    	},
        {
            closeAfterAdd : true,  
            afterSubmit:processAddEdit
        }
        );

    
    
    $("#table_categorias").jqGrid('hideCol',"id");  //escondo las colomnas

   // jQuery("#table_categorias").jqGrid('navButtonAdd','#pager_proyecto');
   
    var last;
    var last_grid;
    
    $("input").change(function () {
     var ord = $("#ORDRE").val();  //aqui estaba codi 
        last = ord;
        
        if(ord) { 
            
            last_grid = $("#table_categorias").jqGrid('getGridParam',ord);
            
            $("#table_categorias").jqGrid('FormToGrid',ord,"#order");
            $("#table_categorias").jqGrid('editRow',ord);  //'editGridRow' me llama al dialogo de 
            //this.disabled = 'true'; 
            jQuery("#save,#cancel").attr("disabled",false); 
            $(this).focus();
            
        }
    });

    $("#save").click( function() {
        
        jQuery("#save,#cancel").attr("disabled",true); 
        jQuery("#edit").attr("disabled",false);
        jQuery("#table_categorias").jqGrid('saveRow',last);
        last_grid=0;


    });
    
    $("#cancel").click( function() { 
        
        //var ord = $("#codi").val(); 
         
        jQuery("#save,#cancel").attr("disabled",true); 
        jQuery("#edit").attr("disabled",false); 
        jQuery("#table_categorias").jqGrid('restoreRow',last).trigger("reloadGrid");
        //$("#table_categorias").jqGrid('FormToGrid',last,"#order");
        $("#table_categorias").jqGrid('GridToForm',last,"#order");
          
       
    });
    
    $("#del").click( function() { 

            var id = $("#table_categorias").jqGrid('getGridParam','selrow'); 

            if(id){
                
                var answer = confirm("Seguro que quiere borrar esta fila ?")
                if (answer){
                    
               
                    jQuery("#table_categorias").jqGrid('delRowData',id); //solo borra local y manda a borrar por ajax, no recarga la tabla
                    //jQuery("#table_categorias").jqGrid('delGridRow',id); //hace una doble llamada a la tabla mas eficiente por ajax
        
                   $.ajax({
                                                type: "POST",
                                                url:  'ajax/categorias.php',
                                                data: 'oper=del&id='+id,
                                                success: function(data) {
        
                                                    alert('borrado del server');
        
        
                                                }
        
                                    }); 
                }
                
            }
            else
                alert('seleccione una fila');

        });
        
        
    $("#add").click( function() { 

           $("#table_categorias").jqGrid("editGridRow","new",{closeAfterAdd : true,  
            afterSubmit:processAddEdit});
            
        });
    

});


$('#tab_sec').css('visibility', 'hidden');  //me esconde la tabla transp hasta que sea seleccionada una empresa

$("#edit").attr("disabled",false);          //no se pq se me clava el true , revisar



});  








</script>

     
    <div id="mysearch"></div>
    <table  id="table_categorias"><tr><td/></tr></table> 
    <div id="pager_categorias"></div>  
    
    
    
    

