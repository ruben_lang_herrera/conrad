<!doctype html>
<html lang="es" style="height: 100% !important;">

<head>
	<meta charset="utf-8"/>
    <meta http-equiv=“Content-Type” content=“text/html; charset=utf-8” />
	<title>Administracion >> Los Muebles de Conrad</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
        
        
 
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui-lightness/jquery-ui-1.8.18.custom.css" />

        <link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/jquery.lightbox-0.5.css" />
        
        
        <script src="js/js/jquery-1.5.2.min.js" type="text/javascript"></script>
        <script src="js/js/i18n/grid.locale-es.js" type="text/javascript"></script>
        <script src="js/js/jquery.jqGrid.min.js" type="text/javascript"></script>
        
        <script src="js/js/jquery.lightbox-0.5.js" type="text/javascript"></script>
        
	<!--[if lt IE 9]> 
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<script src="js/js/hideshow.js" type="text/javascript"></script>
	<script src="js/js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/js/jquery.equalHeight.js"></script>
        
        
        
        
	<script type="text/javascript">            
       $(document).ready(function() {

	   });
    </script>
    </head> 
    
    
    <body style="background-color: white;"> 
    <section id="main">
    <div class="logo" style="margin-left: 480px;">
        <a href="../index.php" title="www.losmueblesdeconrad.com"><img title="www.losmueblesdeconrad.com" width="200" height="200" alt="Los Muebles de Conrad" src="../images/logo-conrad.jpg"/></a>
    </div>
       <form style="margin-top: 100px; margin-left: 35%; padding-left:15px; " method="post" action="login.php" >
            <fieldset style="width:400px; padding-left: 30px; background-color: white !important;"><legend style="margin-left: -30px;">Login</legend>
                <label for="email">Email</label>
                <input type="text" name="email" id="email" style="margin-bottom: 20px;"/>
                <div class="clear"></div>
                <label for="pass">Password</label>
                <input name="pass" id="pass" type="password" style="margin-left: 15px; 
                
                 background-position: 10px 6px;
                border: 1px solid #BBBBBB;
                border-radius: 5px 5px 5px 5px;
                box-shadow: 0 2px 2px #CCCCCC inset, 0 1px 0 #FFFFFF;
                color: #666666;
                display: block;
                float: left;
                height: 20px;
                margin: 0 10px;
                padding-left: 10px;
                width: 200px;                
                "/>
                <div class="clear"></div>
                 <div id="msg_box" style="display:<?php if($_GET['error']==1)echo 'block'; else echo 'none' ?>; margin-top: 20px; ">
                 <h4 class="alert_error">Usuario o Contrase&ntilde;a invalido</h4>
                 </div>   
            </fieldset>
            <button id="submit" name="submit" value="submit">Entrar</button>
       </form>
       
    </section>     



</body>

</html>