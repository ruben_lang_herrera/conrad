<?php
ob_start();
header('Content-type: text/html; charset=utf-8');
session_start();
session_destroy();
header('Location:index.php');
ob_end_flush();
?>
