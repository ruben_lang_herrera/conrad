<?php
session_start();


if(!session_is_registered($_GET['u'])){
    header("location:index.php");
}

include_once 'src/controler.php';

?>
<!doctype html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <!--  charset=iso-8859-1" -->
	<meta charset="utf-8"/>
    
	<title>Administraci&oacute;n Los muebles de Conrad</title>
	<div  disabled="disabled"></div>
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
    
    <link href="upload/fileuploader.css" rel="stylesheet" type="text/css"/>
    <script src="upload/fileuploader.js" type="text/javascript"></script>
    
   	   
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui-lightness/jquery-ui-1.8.18.custom.css" />

        <link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/jquery.lightbox-0.5.css" />
        
        
        <script src="js/js/jquery-1.5.2.min.js" type="text/javascript"></script>
        <script src="js/js/i18n/grid.locale-es.js" type="text/javascript"></script>
        <script src="js/js/jquery.jqGrid.min.js" type="text/javascript"></script>
        
        <script src="js/js/jquery.lightbox-0.5.js" type="text/javascript"></script>
        
	<!--[if lt IE 9]> 
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<script src="js/js/hideshow.js" type="text/javascript"></script>
	<script src="js/js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/js/jquery.equalHeight.js"></script>
       
    <script>        
        function createUploader(seccion){      //para usar seccion se debe usar como parte del Id el nombre de cada tabla
                
                
                idioma="";
                               
                var uploader = new qq.FileUploader({
                    
                    element: document.getElementById(seccion+idioma),    //archivo en espa�ol
                    action: 'upload.php',
                    params: {
                                lugar: seccion                          //carpeta donde iran los archivos dentro de imagenes
                            },
                onComplete: function(id, fileName, responseJSON){
                     var up_name=$('#'+seccion+idioma).attr("up_name");           //el parametro es nombre del campo para llamarlo generico en cada seccion
                     if(responseJSON.success){
                        //Le doy update a la Fila con el nombre del File
                        var ord = $("#table_"+seccion).jqGrid('getGridParam','selrow'); 
                        if($("#table_"+seccion).jqGrid('setRowData',ord,{url:fileName})) //url es el nombre de la columna
                             console.log('Se pudo modificar satisfactoriamente');
                        else
                             console.log('Hubo problemas subiendo el file');                 
                       
                        $("#table_"+seccion).jqGrid('editRow',ord); 
                        $("#table_"+seccion).jqGrid('saveRow',ord);
                        
                        $('.qq-upload-success').fadeOut(2000);
                        
                    }
                    else{
                        var ord = $("#table_"+seccion).jqGrid('getGridParam','selrow');
                        $("#table_"+seccion).jqGrid('saveRow',ord);
                        
                    }
                },
                //debug: true
            });           
                      
        } 
        //window.onload = createUploader;  
          
    </script>   
        
        
	<script type="text/javascript">
            
       $(document).ready(function() {


	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});
                $("#hidden").hide();
        
                
                
                $("h3").click(function(){
                    
                      $('.module_content').html('<div style="text-align: center;"><img src="images/loading.gif"/></div>');
                                      
                      if($(this).attr('class')=='sub')
                        {
                            $("#hidden").show('slow');
                        }
                      else{
                            $("#hidden").hide('slow'); 
                      }
                        $("h3").each(function(){

                                $(this).removeClass("selected");

                        });

                        
                        $(this).addClass("selected");
                        $(".section_title").html($(this).html());
                        $(".current").html($(this).attr("id"));
                        var id_atr = $(this).attr("id");
                        $.ajax({

                                type: "GET",
                                url: 'ajax/ajax.php',
                                data: 'gestion='+$(this).attr("id"),
                                success: function( msg ) {
                                    
                                    $('.module_content').html(msg);
                                    
                                   if(id_atr=='productos1'){
                                        createUploader("productos");
                                      
                                        }
                                   else if(id_atr=='imagenes_slide') {
                                        createUploader("imagenes");
                                        
                                        }
                                   
                                    
                                    
                                }
                        });
            
              
            
                });
                
               

        
});

        
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title">
                <a href="../index.php" title="www.losmueblesdeconrad.com">
                <img  style="max-height: 50px;margin-top: 3px;" src="../images/logo-conrad.png" title="Los Muebles de Conrad" />
                </a>
            </h1>
            <h2 class="section_title">Categor&iacute;as</h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p><?php echo $_GET['u']?></p>
			<a class="logout_user" href="logout.php" title="Logout">Logout</a>
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="admin.php">Admin Website </a> <div class="breadcrumb_divider"></div> <a class="current">Categor&iacute;as</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column" style="height: 500px;" > 
    <div style="height: 30px; padding-left: 20px;">
    <h2 class="selected">SECCIONES</h3>
    </div>
	<!-- search	<form class="quick_search">
			<input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form> -->
		<hr/>
                <a class="href_pointer"><h3 id="categorias" class="selected" >Categorias</h3></a>                
                <a class="href_pointer"><h3 id="productos1">Productos</h3></a>                
                <a class="href_pointer"><h3 id="imagenes_slide">Slide Im&Aacute;genes</h3></a>           
        		
               	
		
		<footer style="margin-top: 100%;">
			<hr />
			
		</footer>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		
		
		<article class="module width_full">
			
			<div class="module_content" style="padding-left: 13%;">
                             <?php 
                                $URL1 = "grid/categorias_grid.php";
                                $content1 = file_get_contents($URL1);
                             ?>   
                            
                           <?php echo $content1; ?>
                           <div class="clear"></div>
                           <div  class="leftc"> </div>
                           <div class="rightc"></div>
                           <!--<div id="tab_sec"></div>-->
                           <div class="clear"></div> 
			</div>
		
			<footer>
				
			</footer>
		</article> 
           
	</section>

<div id="dialog" title="Error"></div>

</body>

</html>