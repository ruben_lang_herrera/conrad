
<?php include 'head.php'; ?>
<?php include('./admin/src/controler.php'); ?>
<?php
	$prod_list = array();
    $prod = new productos();
    
    
    $limit = 6;                                 //son 6 productos por pagina
      
    
    if(isset($_REQUEST['page']))
        $page = $_REQUEST['page'];
    else
        $page = 1; //se inicia la pagina en 1  
          
    
   
   
    //echo 'aki';
    if(isset($_REQUEST['lista']))
    {
        $lista = $_REQUEST['lista'];
        
        $id_cat= $lista;
        $total_prods = $prod->get_total($id_cat);
      
        
        $prod->set_page($page);
        $prod->set_end($limit);
        
        
        if($lista)
            $prod_list=$prod->get_datos('where id_categoria ='.$lista); //Obtengo los productos de la categoria 
        else
            $prod_list=$prod->get_datos(); //Obtengo Todos 
        
        
            
    }
    else    
        {
            $prod_list=$prod->get_datos(); //Obtengo Todos 
            $id_cat = 0;
        }
    
    $last_page = ceil($total_prods/$limit);
?>
<body>
<div class="name">productos</div>
<div class="wrapp_external">
    <div class="wrapp">
        <?php include_once 'header.php'?>
        <article>
            <div class="indicaciones">
                <?php //usando la lista de categorias generada por el menú
                    if($id_cat)
                    {
	                   $list_cat= $cat->get_datos('where id ='.$id_cat);
                       $nombre_cat = decoder_characters_front($list_cat[1]['nombre_'.$LANG]);
                    }
                    else
                        $nombre_cat= $aLang["listaProductos.todos"];
                        
                ?>
                <div class="product-info"><?php echo $aLang["listaProductos.productos"] ?>  </div><div class="separador">/</div><div class="product-info last"><?php echo ' '.$nombre_cat ?></div>
                <div class="clear"></div>
            </div>
            <div class="productos">
            <?php
                for($i=1;$i<=count($prod_list);$i++)
                {
                  echo '<div class="prod">
                            <a href="fichaProductos.php?producto='.$prod_list[$i]['id'].'" title="'.decoder_characters_front($prod_list[$i]['nombre_'.$LANG]).'">
                            <img src="./admin/upload/productos/'.$prod_list[$i]['url'].'"/>
                            </a>';
                            if($prod_list[$i]['vendido']=='Si')
                                echo '<div class="vendido"></div>';
                           else if($prod_list[$i]['nuevo']=='Si')
                                echo '<div class="nuevo"></div>';
                           else 
                                echo '<div class="nada"></div>'; 
                                
                           echo'<div class="blur">
                                    <div class="ref1">'.decoder_characters_front($prod_list[$i]['ref']).'</div>
                                    <div class="dotted"></div>
                                    <div class="nomb">'.decoder_characters_front($prod_list[$i]['nombre_'.$LANG]).'</div>
                                </div>';
                           
                         
                     echo '</div>'; 
                     
                                                                                                                
                }
	
            ?>
                 <div class="clear"></div>
            </div>
            <div class="paginacion">
            <div class="next">
                    <?php 
                        if($last_page!=$page)
                            echo'<a href="listaProductos.php?lista='.$lista.'&page='.($page+1).'">&gt;&gt;</a>';
                    ?>
                </div>
                <?php echo '<div class="paginas"> '.$page.' '.$aLang["listaProductos.de"].' '.$last_page.' </div>'; ?>
                
                <div class="prev">
                    <?php 
                    if($page-1)
                        echo'<a href="listaProductos.php?lista='.$lista.'&page='.($page-1).'">&lt;&lt;</a>';
                    ?>
                </div>
            </div>
            <div class="clear"></div>
        </article>
    </div>
</div>
<footer>
</footer>
</div>
</body>
</html>
