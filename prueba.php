<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=9" />
		<!-- key words -->
		<title>
			Los Muebles de Conrad
		</title>
		<!-- Google Analitycs -->
		<meta name="description" content="Venta de muebles"/>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js">
		</script>
		<script type="text/javascript" src="./js/lupa/featuredimagezoomer.js">
		</script>
		<script type="text/javascript">
			
            $(document).ready(function() {
                
              
               
               
              
                
               
                
            });

		</script>
		<script type="text/javascript">
			

jQuery(document).ready(function($){

 $('.name').hide(); 
 var selected = $('.name').html();
  $("#"+selected).addClass("selected");

})


		</script>
	</head>
	<body>
		<div class="name">
			productos
		</div>
		<div class="wrapp_external">
			<div class="wrapp">
				<header>
					<div class="logo">
						<a href="index1.php" title="Los Muebles de Conrad"><img src="./images/logo-conrad.png" /></a>
					</div>
					<div class="menu">
						<nav class="nav">
							<ul class="principal">
								<li id="conrad" class="linea">
									<a href="conrad.php" title="Quién es Conrad">Quién es Conrad</a>
								</li>
								<li id="productos" class="sub">
									<a href="#">Productos</a>
									<ul class="secundario">
										<li>
											<a href="listaProductos.php?lista=0"><p>Todos</p></a>
										</li>
										<li>
											<a href="listaProductos.php?lista=1"><p>Toma asiento</p></a>
										</li>
										<li>
											<a href="listaProductos.php?lista=2"><p>Iluminaria</p></a>
										</li>
										<li>
											<a href="listaProductos.php?lista=3"><p>Mesas</p></a>
										</li>
										<li>
											<a href="listaProductos.php?lista=4"><p>Variedades</p></a>
										</li>
									</ul>
								</li>
								<li id="proyectos" class="linea">
									<a href="proyectos.php">Proyectos</a>
								</li>
								<li id="condiciones" class="linea">
									<a href="condiciones.php">Condiciones</a>
								</li>
								<li id="contacto" class="linea">
									<a href="contacto.php">Contacto</a>
								</li>
							</ul>
						</nav>
						<div class="social">
							<a><img src="images/facebook-color.png"/></a>
							<a><img src="images/twitter-color.png"/></a>
						</div>
						<div class="idioma">
							<a id="ES" class="selected" href="lang.php?lang=ES">CAS </a>
							|
							<a id="CA" class="" href="lang.php?lang=CA"> CAT</a>
						</div>
						<div class="clear">
						</div>
						<div class="clear">
						</div>
						<div class="line">
						</div>
						<div class="line">
						</div>
				</header>
				<article>
					<div class="indicaciones">
						<div class="product-info">
							Productos
						</div>
						<div class="separador">
							/
						</div>
						<div class="product-info last">
							Todos
						</div>
						<div class="clear">
						</div>
					</div>
					<div class="productos">
						<div class="prod">
							<a href="fichaProductos.php?producto=1" title="PERCHERO METÁLICO DE PIE">
							<img src="./admin/upload/productos/023_Mesitamimbre_red.jpg"/>
							</a>
							<div class="nuevo">
							</div>
                            <div class="blur">
                                <div class="ref1">Ref.00</div>
                                <div class="dotted"></div>
                                <div class="nomb">Silla Nombre</div>
                            </div>
						</div>
						<div class="prod">
							<a href="fichaProductos.php?producto=2" title="MESITA MARRÓN">
							<img src="./admin/upload/productos/023_Mesitamimbre_red.jpg"/>
							</a>
							<div class="nuevo">
							</div>
						</div>
						<div class="prod">
							<a href="fichaProductos.php?producto=8" title="MESITA ROJA">
							<img src="./admin/upload/productos/023_Mesitamimbre_red.jpg"/>
							</a>
							<div class="nuevo">
							</div>
						</div>
						<div class="prod">
							<a href="fichaProductos.php?producto=19" title="MESITA MIMBRE">
							<img src="./admin/upload/productos/023_Mesitamimbre_red.jpg"/>
							</a>
							<div class="vendido">
							</div>
						</div>
						<div class="prod">
							<a href="fichaProductos.php?producto=20" title="SILLA 64 (CESCA)">
							<img src="./admin/upload/productos/023_Mesitamimbre_red.jpg"/>
							</a>
							<div class="vendido">
							</div>
						</div>
						<div class="prod">
							<a href="fichaProductos.php?producto=21" title="otra mesa">
							<img src="./admin/upload/productos/023_Mesitamimbre_red.jpg"/>
							</a>
						</div>
						<div class="clear">
						</div>
					</div>
					<div class="paginacion">
						<div class="next">
							<a href="listaProductos.php?lista=0&page=2">&gt;&gt;</a>
						</div>
						<div class="paginas">
							1 de 2
						</div>
						<div class="prev">
						</div>
					</div>
					<div class="clear">
					</div>
				</article>
				</div>
			</div>
			<footer>
			</footer>
		</div>
	</body>

</html>