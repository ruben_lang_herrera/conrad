<!-- Menu -->
<div id="menu">
            <ul>
                <a href="galeria.php"><li class="unique"><?php echo $aLang['menu.galerias']; ?></li></a>
                <a class="href_pointer"><li class="submenu expo"><?php echo $aLang['menu.exposiciones']; ?></li></a>
                <div class="sub_hide expo">
                    <a class="href_pointer" href="lista_expos.php?expos=actuales"><li><?php echo $aLang['menu.actuales']; ?></li></a>
                    <?php
	                   $c_year = date(Y);
                       for($c_year; $c_year>=2011;$c_year--){
                           echo '<a class="href_pointer" href="lista_expos.php?expos='.$c_year.'"><li>'.$c_year.'-'.($c_year + 1).'</li></a>';
                       }
                    ?>
                </div>
                <a class="href_pointer"><li class="submenu artist"><?php echo $aLang['menu.artistas']; ?></li></a>
                 <div class="sub_hide artist">
                 <?php
	                for($i=1;$i<=count($artistas);$i++)
                         echo '<a href="artistas.php?art='.urlencode($artistas[$i]['nombre']).'"><li>'.decoder_characters_front($artistas[$i]['nombre']).'</li></a>';
                ?>
                 </div>
                <a href="noticias.php"><li class="unique"><?php echo $aLang['menu.news']; ?></li></a>
                <a href="links.php"><li class="unique"><?php echo $aLang['menu.links']; ?></li></a>
                <a href="contactos.php"><li class="unique"><?php echo $aLang['menu.contactos']; ?></li></a>
            </ul>
</div>

<!-- Social Links -->
<div id="social">
            <a href="http://www.facebook.com/pages/Galer%C3%ADa-Bacelos/150598298296931?v=wall" target="_new">
                <img src="images/facebook.png" title="bacelos facebook" width="16" height="16" border="0" />
            </a> 
            <a href="https://twitter.com/galeriabacelos" target="_new">
                <img src="images/twitter.png" title="twitter bacelos" width="16" height="16" border="0" />
            </a>
        </div>