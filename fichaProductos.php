
<?php include 'head.php'; ?>
<?php include('./admin/src/controler.php'); ?>
<?php
  
  if(isset($_GET['producto']))
    $producto = $_GET['producto'];
  else  
    header('Location: '.$_SERVER['HTTP_REFERER']);
  
  $prod_list = array();  
  $pro = new productos();
  
   $cat_list= array();
  $cat= new categorias();
  
  
  $prod_rela = array(); 
  
  $prod_list = $pro->get_datos('where id  ='.$producto); //id_categoria
  $prod_rela = $pro->get_datos('where id != '.$producto.' and id_categoria = '.$prod_list[1]['id_categoria']); //id_categoria
  
  $cat_list= $cat->get_datos('where id = '.$prod_list[1]['id_categoria']);
  
?>

<script type="text/javascript">

jQuery(document).ready(function($){

	$('#image1').addimagezoom({
		zoomrange: [3, 10],
		magnifiersize: [300,300],
		magnifierpos: 'right',
		cursorshade: true,
		largeimage: 'http://i44.tinypic.com/11icnk5.jpg' //<-- No comma after last option!
	})
	
	$('#image2').addimagezoom({
		zoomrange: [5, 5],
		magnifiersize: [400,400],
		magnifierpos: 'right',
		cursorshade: true,
		cursorshadecolor: 'pink',
		cursorshadeopacity: 0.3,
		cursorshadeborder: '1px solid red',
		largeimage: 'http://i44.tinypic.com/11icnk5.jpg' //<-- No comma after last option!
	})

	$('#image3').addimagezoom(
    {
	   zoomrange: [3, 10],
	   magnifiersize: [300,300],
       magnifierpos: 'right',
	   cursorshade: true,
       largeimage:"admin/upload/productos/<?php echo $prod_list[1]['url']?>"
	}
    )

})

</script>
<body>

<div class="name">productos</div>
<div class="wrapp_external_ficha">
    <div class="wrapp_ficha">
        <?php include_once 'header.php'?>
        <article>
            <div class="indicaciones">
                <div class="product-info"><?php echo $aLang['fichaProductos.productos']; ?> </div><div class="product-info"> <?php echo decoder_characters_front($cat_list[1]['nombre_'.$LANG]); ?> / </div><div class="product-info last"><?php echo ' '.decoder_characters_front($prod_list[1]['ref']);?></div>
                <div class="clear"></div>
            </div>
            <?php
                echo '<div class="wrap">  
                    <div class="producto">
                        <img border="0" src="./admin/upload/productos/'.$prod_list[1]['url'].'" id="image3" style="opacity: 1;"/>
                       
                    </div>
                    <!--
                         <img src="images/zoom-in.jpg" class="in">
                         <img src="images/zoom-out.jpg" class="out">
                    -->
                    <div class="caract">
                        <div class="ref">'.decoder_characters_front($prod_list[1]['ref']).'</div>
                        <div class="nombre">'.decoder_characters_front($prod_list[1]['nombre_'.$LANG]).'</div>
                        <p class="descripcion">'.decoder_characters_front($prod_list[1]['descripcion_'.$LANG]).'</p>
                        <div class="datos_label">'.$aLang['fichaProductos.medidas'].'</div><div class="datos">'.decoder_characters_front($prod_list[1]['medidas']).'</div>
                            <div class="clear"></div>
                        <div class="datos_label" >'.$aLang['fichaProductos.stock'].'</div><div class="datos" style="margin-left: 23px;">'.decoder_characters_front($prod_list[1]['stock']).'</div>
                            <div class="clear"></div>
                        <div class="datos_label">'.$aLang['fichaProductos.estado'].'</div><div class="datos" style="margin-left: 18px;">'.decoder_characters_front($prod_list[1]['estado_'.$LANG]).'</div>
                            <div class="clear"></div>
                        <div class="datos_label">'.$aLang['fichaProductos.precio'].'</div><div class="datos" style="margin-left: 18px;">'.decoder_characters_front($prod_list[1]['precio']).'</div>
                            <div class="clear"></div>
                        <div class="reserva">
                        <a href="contacto.php"><img src="images/reservar.jpg" title="'.$aLang["fichaProductos.reservar"].'"/></a>
                        </div>
                    </div>
                    
                    <div class="clear"></div>
                    
                </div>';
	
            ?>
            <div class="relacionados txt-titulo2">
            <p class="titulo"><?php echo $aLang['fichaProductos.relacionados'] ?></p>
            
            <?php
	             for($i=1;$i<=count($prod_rela);$i++)  
                 {
                    
                    if($i<=5){ //limito a 5 productos maximo
                    
                        echo    '<div class="prod_rel">
                                    <a href="fichaProductos.php?producto='.$prod_rela[$i]['id'].'" title="'.decoder_characters_front($prod_rela[$i]['nombre_'.$LANG]).'">
                                        <img src="./admin/upload/productos/'.decoder_characters_front($prod_rela[$i]['url']).'" />
                                    </a>
                                </div>';
                            }
                 }
            ?>
            <div class="clear"></div>
            </div>
            </div>
        </article>
    </div>
</div>
<footer>
</footer>
</div>
</body>
</html>
